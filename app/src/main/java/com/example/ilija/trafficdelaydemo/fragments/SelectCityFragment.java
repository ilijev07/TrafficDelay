package com.example.ilija.trafficdelaydemo.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.ilija.trafficdelaydemo.R;
import com.example.ilija.trafficdelaydemo.listeners.SpinnerOnItemSelectedListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SelectCityFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SelectCityFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SelectCityFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Spinner spinnerCities;



    private OnFragmentInteractionListener mListener;

    public SelectCityFragment() {
        // Required empty public constructor

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SelectCityFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SelectCityFragment newInstance(String param1, String param2) {
        SelectCityFragment fragment = new SelectCityFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
        Log.w("SelectCityFragment ", "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_select_city, container, false);
        addItemsOnSpinner(view);
        addListenerOnSpinnerItemSelection(view);
        Log.w("SelectCityFragment ", "onCreateView");

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        Log.w("SelectCityFragment ", "onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        Log.w("DEBUG:","SelectCityFragment onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.w("DEBUG:","SelectCityFragment onStart");
        super.onStart();
    }
    @Override
    public void onResume() {

        Log.w("DEBUG:","SelectCityFragment onResume");
        super.onResume();
    }


    @Override
    public void onPause() {
        Log.w("DEBUG:","SelectCityFragment onPause");
        super.onPause();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    public void addItemsOnSpinner(View view){

        String [] cities = getResources().getStringArray(R.array.cities);
        final List<String>  list = new ArrayList<>(Arrays.asList(cities));
        spinnerCities = (Spinner) view.findViewById(R.id.spinnerCities);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String> (getContext(), android.R.layout.simple_spinner_item, list)
        {
            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                if (convertView == null) {
                    LayoutInflater inflater = LayoutInflater.from(getContext());
                    convertView = inflater.inflate(
                            android.R.layout.simple_spinner_item, parent, false);
                }
                // android.R.id.text1 is default text view in resource of the android.
                // android.R.layout.simple_spinner_item is default layout in resources of android.

                TextView tv = (TextView) convertView
                        .findViewById(android.R.id.text1);
                tv.setText(list.get(position));
                tv.setGravity(Gravity.CENTER);
                tv.setTextSize(25);
                tv.setTextColor(Color.BLACK);
                tv.setTypeface(null, Typeface.BOLD);

                return convertView;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent)
            {
                if (convertView == null) {
                    LayoutInflater inflater = LayoutInflater.from(getContext());
                    convertView = inflater.inflate(
                            android.R.layout.simple_spinner_item, parent, false);
                }

                TextView tv = (TextView) convertView
                        .findViewById(android.R.id.text1);
                tv.setText(list.get(position));
                tv.setGravity(Gravity.CENTER);
                tv.setTextColor(Color.BLACK);
                tv.setTextSize(25);
                tv.setTypeface(null, Typeface.BOLD);

                return convertView;

            }
        };

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerCities.setAdapter(dataAdapter);
        spinnerCities.setPrompt("Select a city prompt");
    }
    public void addListenerOnSpinnerItemSelection(View view) {
        spinnerCities = (Spinner) view.findViewById(R.id.spinnerCities);
        spinnerCities.setOnItemSelectedListener(new SpinnerOnItemSelectedListener(getActivity(),spinnerCities));
    }

}
