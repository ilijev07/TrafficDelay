package com.example.ilija.trafficdelaydemo;

import android.app.Application;
import android.content.Context;

/**
 * Created by Ilija on 24-Jul-17.
 */

public class MyApplication extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }
}
