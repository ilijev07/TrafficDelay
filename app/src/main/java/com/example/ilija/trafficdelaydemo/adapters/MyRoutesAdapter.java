package com.example.ilija.trafficdelaydemo.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ilija.trafficdelaydemo.R;
import com.example.ilija.trafficdelaydemo.RoutesActivity;
import com.example.ilija.trafficdelaydemo.ViewRouteActivity;
import com.example.ilija.trafficdelaydemo.model.Route;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilija on 12-Aug-17.
 */

public class MyRoutesAdapter extends BaseAdapter {




    private Context context;
    private List<Route> items;


    public MyRoutesAdapter(Context context, List<Route> items) {
        super();
        this.context = context;
        this.items = items;
    }

    /**
     * Holder for the list items.
     */
    private class ViewHolder{
        TextView startRoute;
        TextView endRoute;
        TextView cityName;
        Button buttonView;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void replacePlaces(ArrayList<Route> routes) {
        this.items = routes;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        Route item = (Route) getItem(position);
        View viewToUse = null;

        // This block exists to inflate the settings list item conditionally based on whether
        // we want to support a grid or list view.
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {

            viewToUse = mInflater.inflate(R.layout.fragment_route_item, null);


            holder = new ViewHolder();
            holder.startRoute = (TextView)viewToUse.findViewById(R.id.startPoint);
            holder.endRoute = (TextView)viewToUse.findViewById(R.id.endPoint);
            holder.cityName = (TextView) viewToUse.findViewById(R.id.selectedCity);
            holder.buttonView = (Button)viewToUse.findViewById(R.id.viewRouteButton);
            viewToUse.setTag(holder);
        } else {
            viewToUse = convertView;
            holder = (ViewHolder) viewToUse.getTag();
        }

        holder.startRoute.setText(item.getStartPoint());
        holder.endRoute.setText(item.getEndPoint());
        holder.cityName.setText(item.getCityName());
        holder.buttonView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //TODO UPDATE the model here
                // here open new google maps activity
                // Start ViewRouteActivity.class
                Intent myIntent = new Intent(context,
                        ViewRouteActivity.class);
                myIntent.putExtra("startPoint",holder.startRoute.getText());
                myIntent.putExtra("endPoint",holder.endRoute.getText());
                myIntent.putExtra("cityName",holder.cityName.getText());
                context.startActivity(myIntent);

               // Toast.makeText(context,"Clicked item is "+holder.startRoute.getText(), Toast.LENGTH_LONG).show();
            }
        });
        return viewToUse;
    }
}
