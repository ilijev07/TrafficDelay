package com.example.ilija.trafficdelaydemo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.StringBuilderPrinter;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ilija.trafficdelaydemo.database.DatabaseHandler;
import com.example.ilija.trafficdelaydemo.helpers.MapHttpConnection;
import com.example.ilija.trafficdelaydemo.helpers.PathJSONParser;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ViewRouteActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Toolbar myToolbar;
    private String startPoint;
    private String endPoint;
    private String cityName;
    private ProgressBar initialProgressBar;
    private ProgressBar calculateProgressBar;
    private LinearLayout mainContent;
    private static final int overview = 0;
    private DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_route);
        initialProgressBar = (ProgressBar) findViewById(R.id.viewRouteProgressBar);
        calculateProgressBar = (ProgressBar) findViewById(R.id.progressBarCalculateRouteDelay);
        initialProgressBar.setVisibility(View.VISIBLE);
        mainContent = (LinearLayout) findViewById(R.id.viewRouteMainContent);
        db = new DatabaseHandler(this);
        // setting toolbar and back button
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        checkBackStack();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // getting bundle arguments
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            startPoint = extras.getString("startPoint");
            endPoint = extras.getString("endPoint");
            cityName = extras.getString("cityName");
        }

        // Setting name of the city which is equal with endPoint of the route
        if(cityName != null){
            TextView tvNameOfCity = (TextView) findViewById(R.id.cityNameTop);
            tvNameOfCity.setText(cityName);
        }
        if(!isNetworkAvailable(this)){
            Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show();
        }
        else{
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }


        Switch toggleTrafficLines = (Switch) findViewById(R.id.toggleTrafficLines);
        toggleTrafficLines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // enabling/ disabling traffic layer on map
                mMap.setTrafficEnabled(isChecked);
            }
        });

        Button calculateDelayButton = (Button) findViewById(R.id.calculateDelayButton);

        // Capture calculate button clicks
        calculateDelayButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                calculateProgressBar.setVisibility(View.VISIBLE);
                if(!isNetworkAvailable(ViewRouteActivity.this)){
                    Toast.makeText(ViewRouteActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
                }
                else {
                    new CalculateDelay(arg0.getContext()).execute(startPoint,endPoint);
                }
            }
        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        new GetLatLong(this).execute(startPoint,endPoint);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);
        // here check if route is from database

        if(db.checkIfExists(startPoint,endPoint)){
            menu.findItem(R.id.action_delete).setVisible(true);
        }

        return super.onCreateOptionsMenu(menu);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
            // action with ID action_refresh was selected
            case R.id.action_refresh:
                finish();
                startActivity(getIntent());
                break;
            // action with ID action_delete was selected
            case R.id.action_delete:
                deleteRouteMethod(startPoint,endPoint);
                onBackPressed();
               // here function call for deleting elemet if is from database
                break;
            // action with ID action_settings was selected
            case R.id.action_settings:
                break;
            default:
                break;
        }

        return true;
    }
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }
    }
    public void checkBackStack(){
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true); // show back button
                    myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                } else {
                    //show hamburger
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);

                    myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                }
            }
        });
    }
    class GetLatLong extends AsyncTask<String,Integer,ArrayList<Address>> {
        private Context context;

        public GetLatLong(Context context){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {


        }

        @Override
        protected ArrayList<Address> doInBackground(String... params) {
            //Do some task
            ArrayList<Address> result = new ArrayList<>();
            Address address = null;
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List addressListStart = null;
            List addressListEnd = null;
            try {
                addressListStart = geocoder.getFromLocationName(params[0], 1);
                addressListEnd = geocoder.getFromLocationName(params[1], 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addressListStart != null && addressListStart.size() > 0) {
                address = (Address) addressListStart.get(0);
                result.add(address);
            }
            if (addressListEnd != null && addressListEnd.size() > 0) {
                address = (Address) addressListEnd.get(0);
                result.add(address);
            }

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Address> addresses) {
            //Show the result obtained from doInBackground


            // here result is arrayList with two addresses i.e object where we can get Latitude and Longitude

            if(addresses.size() > 1 ){
                String url = getMapsApiDirectionsUrl(addresses.get(0), addresses.get(1));
                ReadTask downloadTask = new ReadTask(context);
                // Start downloading json data from Google Directions API
                downloadTask.execute(url);
            }
            else {
                Toast.makeText(context,"Could not get data from Google Services. Please touch refresh",Toast.LENGTH_SHORT).show();
            }
        }
    }
    private boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    // get Url for getting data
    private String  getMapsApiDirectionsUrl(Address origin,Address dest) {
        // Origin of route
        String str_origin = "origin="+origin.getLatitude()+","+origin.getLongitude();

        // Destination of route
        String str_dest = "destination="+dest.getLatitude()+","+dest.getLongitude();


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;


        return url;

    }
    private class ReadTask extends AsyncTask<String, Void , String> {

        private Context context;

        public ReadTask(Context context){
            this.context = context;
        }
        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String data = "";
            try {
                MapHttpConnection http = new MapHttpConnection();
                data = http.readUr(url[0]);


            } catch (Exception e) {
                // TODO: handle exception
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask(context).execute(result);
        }

    }
    private class ParserTask extends AsyncTask<String,Integer, List<List<HashMap<String , String >>>> {
        private Context context;

        public ParserTask(Context context){
            this.context = context;
        }
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {
            // TODO Auto-generated method stub
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            LatLng startLatLng = null, endLatLng = null;
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    if(j == 0){
                        startLatLng = position;
                    }
                    if(j == path.size()-1 ){
                        endLatLng = position;
                    }
                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(12);
                polyLineOptions.color(Color.BLUE);
            }


            if(startLatLng != null && endLatLng !=null && polyLineOptions !=null){
                // add start position on map
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(startLatLng);
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
                markerOptions.title("Start point");
                Marker locationMarkerStart = mMap.addMarker(markerOptions);
                locationMarkerStart.showInfoWindow();

                // add end position on map
                markerOptions = new MarkerOptions();
                markerOptions.position(endLatLng);
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                markerOptions.title("End point");
                Marker locationMarkerEnd = mMap.addMarker(markerOptions);

                // draw polyline on map
                mMap.addPolyline(polyLineOptions);

                // move camera to the points
                mMap.moveCamera(CameraUpdateFactory.newLatLng(startLatLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(12));

                initialProgressBar.setVisibility(View.GONE);
                mainContent.setVisibility(View.VISIBLE);
            }
            else {
                Toast.makeText(context,"Cannot get data from google services, please hit refresh",Toast.LENGTH_LONG).show();
            }
        }
    }
    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext
                .setQueryRateLimit(3)
                .setApiKey(getString(R.string.google_key))
                .setConnectTimeout(3, TimeUnit.SECONDS)
                .setReadTimeout(3, TimeUnit.SECONDS)
                .setWriteTimeout(3, TimeUnit.SECONDS);
    }
    private DirectionsResult getDirectionsDetails(String origin, String destination, TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode)
                    .origin(origin)
                    .destination(destination)
                    .departureTime(now)
                    .await();
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static String handleDirectionResults(DirectionsResult results){
/*
        System.out.println("Duration: "+results.routes[overview].legs[overview].duration.humanReadable);
        System.out.println("Duration in traffic: "+results.routes[overview].legs[overview].durationInTraffic.humanReadable);*/
        long durationSec = results.routes[overview].legs[overview].duration.inSeconds;
        long durationInTrafficSec = results.routes[overview].legs[overview].durationInTraffic.inSeconds;
        long differenceSec = durationInTrafficSec - durationSec;

        if(differenceSec < 0){
            differenceSec = 0;
        }
        return convertSecondsToHoursAndMinutes(differenceSec);
    }
    public static String convertSecondsToHoursAndMinutes(long differenceSec){

       long  hours = differenceSec / 3600;
       long  minutes = (differenceSec % 3600) / 60;
       long seconds = differenceSec % 60;

        return String.format(Locale.US,"%d:%d:%d", hours, minutes, seconds);
    }

    private class CalculateDelay extends AsyncTask<String,Void,String> {
        private Context context;

        public CalculateDelay(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            //Do some task
            String result = null;
            DirectionsResult results = getDirectionsDetails(params[0],params[1],TravelMode.DRIVING);
            if(results != null){
                result = handleDirectionResults(results);
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            //Show the result obtained from doInBackground

            if(result != null){
                TextView averageDelayTextView = (TextView) findViewById(R.id.averageDelayRouteTxt);
                StringBuilder sb = new StringBuilder();
                // we split result --- parts[0] hours, parts[1] minutes, parts[2] seconds
                String parts [] = result.split(":");
                // hours are not zero
                if(Integer.parseInt(parts[0]) != 0){
                    sb.append(parts[0]+ "hours ");
                }
                // minutes are not zero
                if(Integer.parseInt(parts[1]) != 0){
                    sb.append(parts[1]+" minutes ");
                }
                // seconds are not zero
                if(Integer.parseInt(parts[2]) != 0){
                    sb.append(parts[2]+" seconds ");
                }
                if(sb.toString().length() == 0){
                    sb.append("Delay for this route is zero");
                }
                averageDelayTextView.setText(sb.toString());

                calculateProgressBar.setVisibility(View.GONE);
            }
            else {
                Toast.makeText(context,"Could not get data from Google Services. Please hit calculate again",Toast.LENGTH_SHORT).show();
            }
        }
    }
    public void deleteRouteMethod(String startPoint,String endPoint){
        if(db.deleteRoute(startPoint,endPoint)){
            Toast.makeText(ViewRouteActivity.this,"Route successfully deleted",Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(ViewRouteActivity.this,"Could not get remove route, please try again",Toast.LENGTH_SHORT).show();
        }
    }
}
