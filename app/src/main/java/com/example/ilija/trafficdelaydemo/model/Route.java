package com.example.ilija.trafficdelaydemo.model;

/**
 * Created by Ilija on 09-Aug-17.
 */

public class Route {

    private int id;

    private String startPoint;

    private String endPoint;

    private String cityName;


    public Route(){

    }
    public Route(int id, String startPoint,String endPoint,String cityName){

        this.id = id;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.cityName = cityName;
    }
    public Route(String startPoint,String endPoint,String cityName){

        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.cityName = cityName;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return this.id;
    }

    public void setStartPoint(String startPoint){
        this.startPoint = startPoint;
    }

    public void setEndPoint(String endPoint){
        this.endPoint = endPoint;
    }

    public void setCityName(String cityName){
        this.cityName = cityName;
    }

    public String getStartPoint(){
        return startPoint;
    }

    public String getEndPoint(){
        return endPoint;
    }

    public String getCityName(){
        return cityName;
    }


    @Override
    public String toString() {
        return String.format("Start point: %s, End point: %s - City: %s",startPoint,endPoint,cityName);
    }
}