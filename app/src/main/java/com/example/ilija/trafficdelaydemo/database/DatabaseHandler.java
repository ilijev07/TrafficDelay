package com.example.ilija.trafficdelaydemo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.ilija.trafficdelaydemo.model.Route;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilija on 09-Aug-17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 5;

    // Database Name
    private static final String DATABASE_NAME = "trafficDelay";

    // Contacts table name
    private static final String TABLE_ROUTES = "routes";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_START_POINT = "start_point";
    private static final String KEY_END_POINT = "end_point";
    private static final String KEY_CITY_NAME = "city_name";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ROUTES_TABLE = "CREATE TABLE " + TABLE_ROUTES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_START_POINT + " TEXT,"
                + KEY_END_POINT + " TEXT,"  + KEY_CITY_NAME + " TEXT" + " )";
        db.execSQL(CREATE_ROUTES_TABLE);
        Log.d("DatabaseHandler","onCreate");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("DatabaseHandler","onUpgrade");
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROUTES);

        // Create tables again
        onCreate(db);

    }

    public void addRoute(Route route) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_START_POINT, route.getStartPoint()); // Route start point
        values.put(KEY_END_POINT, route.getEndPoint()); // Route end point
        values.put(KEY_CITY_NAME, route.getCityName()); // Route city name

        // Inserting Row
        db.insert(TABLE_ROUTES, null, values);
        db.close(); // Closing database connection
    }

    public Route getRoute(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ROUTES, new String[] { KEY_ID,
                        KEY_START_POINT, KEY_END_POINT, KEY_CITY_NAME }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Route route = new Route(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3));


        cursor.close();
        db.close(); // Closing database connection

        // return route
        return route;
    }

    public ArrayList<Route> getAllRoutes() {
        ArrayList<Route> routeList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ROUTES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Route route = new Route();
                route.setId(Integer.parseInt(cursor.getString(0)));
                route.setStartPoint(cursor.getString(1));
                route.setEndPoint(cursor.getString(2));
                route.setCityName(cursor.getString(3));
                // Adding route to list
                routeList.add(route);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        // return route list
        return routeList;
    }

    public ArrayList<Route> getRoutesForCity(String city) {
        ArrayList<Route> routeList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ROUTES + " WHERE " + KEY_CITY_NAME + " = @city";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{city});

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Route route = new Route();
                route.setId(Integer.parseInt(cursor.getString(0)));
                route.setStartPoint(cursor.getString(1));
                route.setEndPoint(cursor.getString(2));
                route.setCityName(cursor.getString(3));
                // Adding route to list
                routeList.add(route);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        // return route list
        return routeList;
    }

    public int getRoutesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ROUTES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    public int updateRoute(Route route) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_START_POINT, route.getStartPoint());
        values.put(KEY_END_POINT, route.getEndPoint());
        values.put(KEY_CITY_NAME, route.getCityName());


        // updating row
        return db.update(TABLE_ROUTES, values, KEY_ID + " = ?",
                new String[] { String.valueOf(route.getId()) });
    }

/*    public void deleteRoute(Route route) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ROUTES, KEY_ID + " = ?",
                new String[] { String.valueOf(route.getId()) });
        db.close();
    }*/

    public boolean deleteRoute(String startPoint, String endPoint) {
        SQLiteDatabase db = this.getWritableDatabase();
        int returnValue = db.delete(TABLE_ROUTES, KEY_START_POINT + " = ? AND " + KEY_END_POINT + " = ?",
                new String[] { startPoint,  endPoint });
        db.close();
        return returnValue > 0;
    }

    public boolean checkIfExists(String startPoint, String endPoint){
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "SELECT * FROM " + TABLE_ROUTES + " WHERE " + KEY_START_POINT + " = ? AND " + KEY_END_POINT + " = ?";
        Cursor cursor = db.rawQuery(Query, new String[] { startPoint,  endPoint });
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

}
