package com.example.ilija.trafficdelaydemo.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ilija.trafficdelaydemo.NewRouteActivity;
import com.example.ilija.trafficdelaydemo.R;
import com.example.ilija.trafficdelaydemo.RoutesActivity;
import com.example.ilija.trafficdelaydemo.ViewRouteActivity;
import com.example.ilija.trafficdelaydemo.adapters.MyRoutesAdapter;
import com.example.ilija.trafficdelaydemo.database.DatabaseHandler;
import com.example.ilija.trafficdelaydemo.model.CityDto;
import com.example.ilija.trafficdelaydemo.model.Route;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TravelMode;


import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RouteListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RouteListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RouteListFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    private ProgressBar initialProgressBar;
    private String selectedCity;
    private LinearLayout listViewLayout;
    protected MyRoutesAdapter routesAdapter;
    private ProgressBar progressBarCalculate;
    private  ArrayList<Route> allRoutes;
    private static final int overview = 0;
    public RouteListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RouteListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RouteListFragment newInstance(String param1, String param2) {
        RouteListFragment fragment = new RouteListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedCity = getArguments().getString("selectedCity");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_route_list, container, false);
        listViewLayout = (LinearLayout) view.findViewById(R.id.listViewLayout);
        listViewLayout.setVisibility(View.GONE);
        initialProgressBar = (ProgressBar) view.findViewById(R.id.initialProgressBar);
        initialProgressBar.setVisibility(View.VISIBLE);
        allRoutes = null;
        progressBarCalculate = (ProgressBar) view.findViewById(R.id.progressBarCalculate);

        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        Log.w("DEBUG:","DetailsCityFragment onActivityCreated");
        if(!isNetworkAvailable(getActivity())){
            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
        }
        else{
            // get Lat Long async task
            new GetLatLong(getActivity()).execute(selectedCity);
        }
        if(selectedCity != null){
            TextView cityTop = (TextView) getActivity().findViewById(R.id.routeListCityTop);
            cityTop.setText(selectedCity);
        }

        Button calculateDelayButton = (Button) getActivity().findViewById(R.id.calculateButton);

        // Capture calculate button clicks
        calculateDelayButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Toast.makeText(getActivity(),"This could take a while, please be patient",Toast.LENGTH_SHORT).show();
                if(allRoutes != null){
                    progressBarCalculate.setVisibility(View.VISIBLE);
                    if(!isNetworkAvailable(getActivity())){
                        Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
                    }
                    else {
                       /// here call of async task
                        new CalculateRoutesDelay(getActivity()).execute(allRoutes);
                    }
                }
                else {
                    Toast.makeText(getActivity(), "Routes are not generated correctly, please hit refresh", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    class GetLatLong extends AsyncTask<String,Integer,Address> {
        private Context context;

        public GetLatLong(Context context){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Address doInBackground(String... params) {
            //Do some task
            Address address = null;
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List addressList = null;
            try {
                addressList = geocoder.getFromLocationName(params[0], 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addressList != null && addressList.size() > 0) {
                address = (Address) addressList.get(0);
            }

            publishProgress (1);

            return address;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            //Update the progress of current task
        }
        @Override
        protected void onPostExecute(Address latLng) {
            //Show the result obtained from doInBackground
            if(latLng == null){
                Toast.makeText(getActivity(), "Could not generate random routes, try again", Toast.LENGTH_SHORT).show();
            }
            else {
                new GenerateAndRetrieveRoutes(getActivity()).execute(latLng);
            }

        }
    }


    class GenerateAndRetrieveRoutes extends AsyncTask<Address,Integer,ArrayList<Route>> {
        private Context context;

        private DatabaseHandler db;

        private String citySelected;

        private ListView listView;

        public GenerateAndRetrieveRoutes(Context context){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            citySelected = selectedCity;
            db = new DatabaseHandler(context);
        }

        @Override
        protected ArrayList<Route> doInBackground(Address... addresses) {
            //Do some task
            allRoutes = new ArrayList<>();
            // get all Routes from database
            ArrayList<Route> routesFromDb = db.getRoutesForCity(citySelected);

            // check if database has routes, if it has add routes to result
            if(routesFromDb != null && routesFromDb.size() != 0){
                for(int i=0;i<routesFromDb.size();i++){
                    allRoutes.add(routesFromDb.get(i));
                }
            }
            // get End Point, that is parameter from input
            Address endLatLng = addresses[0];

            // generate random lattitue longtitue based on input value
            ArrayList<LatLng> randomPoints = generateTenRandomRoutes(endLatLng);
            ArrayList<String> mappedAdresses = new ArrayList<>();

            // map lat long coordinates to address name and add to mappedAdress
             for(int i = 0 ; i < randomPoints.size(); i++){
                 // Make address from latitude and longtitude
                 List<Address> address;
                 Geocoder  geocoder = new Geocoder(context, Locale.getDefault());

                 try {
                     address = geocoder.getFromLocation(randomPoints.get(i).latitude, randomPoints.get(i).longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                     String nameOfAddress = "";
                    // System.out.println("Komplet Adresa: "+address.toString());
                     if(address.size() > 0){
                          nameOfAddress = address.get(0).getAddressLine(0);
                     }

                     mappedAdresses.add(nameOfAddress);
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
            }

            // create new routes and add to result with data generated above
            for(int i = 0; i < mappedAdresses.size(); i++){
                allRoutes.add(new Route(mappedAdresses.get(i),endLatLng.getAddressLine(0),citySelected));
            }




            publishProgress (1);

            return allRoutes;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            //Update the progress of current task
        }

        @Override
        protected void onPostExecute(ArrayList<Route> allRoutes) {
            //Show the result obtained from doInBackground

            // here represent data

            initialProgressBar.setVisibility(View.GONE);

            for(int i=0; i < allRoutes.size(); i++){
                    System.out.println(allRoutes.get(i).toString());
            }
            // here append routes
            if(getView()!= null ){
                listView = (ListView) getView().findViewById(R.id.routeListView);
                routesAdapter = new MyRoutesAdapter(context,allRoutes);
                listView.setAdapter(routesAdapter);
                listViewLayout.setVisibility(View.VISIBLE);
            }


        }
        // generate random points entering first LONGITUDE then LATITUDE
        private LatLng generateRandomLocation(double x0, double y0, int radius) {
            Random random = new Random();

            // Convert radius from meters to degrees
            double radiusInDegrees = radius / 111000f;

            double u = random.nextDouble();
            double v = random.nextDouble();
            double w = radiusInDegrees * Math.sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.cos(t);
            double y = w * Math.sin(t);

            // Adjust the x-coordinate for the shrinking of the east-west distances
            double new_x = x / Math.cos(Math.toRadians(y0));

            double foundLongitude = new_x + x0;
            double foundLatitude = y + y0;


         //   System.out.println("Longitude: " + foundLongitude + "  Latitude: " + foundLatitude );

            return new LatLng(foundLatitude,foundLongitude);

        }

        private ArrayList<LatLng> generateTenRandomRoutes(Address address){

            ArrayList<LatLng> result = new ArrayList<>();

            for(int i = 1000; i <= 10000 ;i+=1000){
                LatLng newLatLng = generateRandomLocation(address.getLongitude(),address.getLatitude(), i);
                result.add(newLatLng);
            }
            return result;

        }
     }
    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext
                .setQueryRateLimit(3)
                .setApiKey(getString(R.string.google_key))
                .setConnectTimeout(3, TimeUnit.SECONDS)
                .setReadTimeout(3, TimeUnit.SECONDS)
                .setWriteTimeout(3, TimeUnit.SECONDS);
    }
    private DirectionsResult getDirectionsDetails(String origin, String destination, TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode)
                    .origin(origin)
                    .destination(destination)
                    .departureTime(now)
                    .await();
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    private long handleDirectionResults(DirectionsResult results){
        long differenceSec = 0;

        if(results.routes[overview].legs[overview].durationInTraffic != null && results.routes[overview].legs[overview].duration != null){
           /* System.out.println("Duration: "+results.routes[overview].legs[overview].duration.humanReadable);
            System.out.println("Duration in traffic: "+results.routes[overview].legs[overview].durationInTraffic.humanReadable);*/
            long durationSec = results.routes[overview].legs[overview].duration.inSeconds;
            long durationInTrafficSec = results.routes[overview].legs[overview].durationInTraffic.inSeconds;
            differenceSec = durationInTrafficSec - durationSec;

            if(differenceSec < 0){
                differenceSec = 0;
            }
        }

        return differenceSec;
    }
    private class CalculateRoutesDelay extends AsyncTask<ArrayList<Route>,Void,String> {
        private Context context;

        public CalculateRoutesDelay(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(ArrayList<Route>... params) {
            //Do some task
            String result = null;

            if(params != null && params.length != 0){
                long sumOfDelay =0;

                for(int i = 0;i < params[0].size(); i++){
                    DirectionsResult results = getDirectionsDetails(params[0].get(i).getStartPoint(),params[0].get(i).getEndPoint(), TravelMode.DRIVING);
                    sumOfDelay += handleDirectionResults(results);
                }
                long toCalculate = sumOfDelay / params[0].size();
                result = ViewRouteActivity.convertSecondsToHoursAndMinutes(toCalculate);
            }


            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            //Show the result obtained from doInBackground

            if(result != null){
                TextView averageDelayTextView = (TextView) getActivity(). findViewById(R.id.averageDelay);
                StringBuilder sb = new StringBuilder();
                // we split result --- parts[0] hours, parts[1] minutes, parts[2] seconds
                String parts [] = result.split(":");
                // hours are not zero
                if(Integer.parseInt(parts[0]) != 0){
                    sb.append(parts[0]+ "hours ");
                }
                // minutes are not zero
                if(Integer.parseInt(parts[1]) != 0){
                    sb.append(parts[1]+" minutes ");
                }
                // seconds are not zero
                if(Integer.parseInt(parts[2]) != 0){
                    sb.append(parts[2]+" seconds ");
                }
                if(sb.toString().length() == 0){
                    sb.append("Delay for all routes is zero");
                }
                averageDelayTextView.setText(sb.toString());

                progressBarCalculate.setVisibility(View.GONE);
            }
            else {
                Toast.makeText(context,"Could not get data from Google Services. Please hit calculate again",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
