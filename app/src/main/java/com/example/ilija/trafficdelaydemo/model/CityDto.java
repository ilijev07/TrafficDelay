package com.example.ilija.trafficdelaydemo.model;

/**
 * Created by Ilija on 03-Aug-17.
 */

public class CityDto {

    private String label;

    private String countryName;

    private String populationTotal;

    private String thumbnail;

    private String comment;

    public CityDto() {

    }

    public CityDto(String label, String countryName, String populationTotal, String thumbnail, String comment) {
        super();
        this.label = label;
        this.countryName = countryName;
        this.populationTotal = populationTotal;
        this.thumbnail = thumbnail;
        this.comment = comment;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getPopulationTotal() {
        return populationTotal;
    }

    public void setPopulationTotal(String populationTotal) {
        this.populationTotal = populationTotal;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {

        return String.format("Label: %s \nCountry name: %s \nPopulation total: %s \nThumbnail string: %s \nComment: %s", label, countryName, populationTotal, thumbnail, comment);
    }
}