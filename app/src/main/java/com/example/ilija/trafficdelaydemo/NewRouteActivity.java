package com.example.ilija.trafficdelaydemo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ilija.trafficdelaydemo.database.DatabaseHandler;
import com.example.ilija.trafficdelaydemo.helpers.MapHttpConnection;
import com.example.ilija.trafficdelaydemo.helpers.PathJSONParser;
import com.example.ilija.trafficdelaydemo.model.Route;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class NewRouteActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Toolbar myToolbar;
    private String selectedCity;
    private ProgressBar initialProgressBar;
    private ArrayList<LatLng> markerPoints;
    private LinearLayout mainContent;
    private Button insertRouteButton;
    private DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_route);

        initialProgressBar = (ProgressBar) findViewById(R.id.newRouteProgressBar);
        mainContent = (LinearLayout) findViewById(R.id.newRouteMainContent);
        insertRouteButton = (Button) findViewById(R.id.insertRouteDelayButton);
        mainContent.setVisibility(View.GONE);
        initialProgressBar.setVisibility(View.VISIBLE);
        db = new DatabaseHandler(this);
        markerPoints = new ArrayList<>();
        // setting toolbar and back button
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        checkBackStack();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // getting bundle arguments
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            selectedCity = extras.getString("selectedCity");
        }

        // Setting name of the city
        if(selectedCity != null){
            TextView tvNameOfCity = (TextView) findViewById(R.id.cityNameNewRoute);
            tvNameOfCity.setText(selectedCity);
        }
        if(!isNetworkAvailable(this)){
            Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show();
        }
        else{
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.newRouteMap);
            mapFragment.getMapAsync(this);
        }

        // Capture button clicks
        insertRouteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                // here call for getting address name from start and end lat long and inserting route into database
                if(isConnected(arg0.getContext())){
                    if(markerPoints != null && markerPoints.size() != 0){
                        new GetAddressAndInsertRoute(arg0.getContext()).execute(markerPoints.get(0),markerPoints.get(1));
                    }
                    else {
                        Toast.makeText(arg0.getContext(),"Cannot insert route into database, please try again",Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(arg0.getContext(),"No internet connection, please turn on",Toast.LENGTH_LONG).show();
                }

            }
        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        new GetLatLong(this).execute(selectedCity);
        // Setting onclick event listener for the map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                // Already two locations
                if (markerPoints.size() > 1) {
                    markerPoints.clear();
                    mMap.clear();
                    insertRouteButton.setEnabled(false);
                }
                // Adding new item to the ArrayList
                markerPoints.add(point);

                // Creating MarkerOptions
                MarkerOptions options = new MarkerOptions();

                // Setting the position of the marker
                options.position(point);

                /**
                 * For the start location, the color of marker is GREEN and
                 * for the end location, the color of marker is RED.
                 */
                if (markerPoints.size() == 1) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    options.title("Start point");
                    insertRouteButton.setEnabled(false);
                } else if (markerPoints.size() == 2) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    options.title("End point");
                    insertRouteButton.setEnabled(true);
                }


                // Add new marker to the Google Map Android API V2
                mMap.addMarker(options);

                // Checks, whether start and end locations are captured
                if (markerPoints.size() >= 2) {
                    LatLng origin = markerPoints.get(0);
                    LatLng dest = markerPoints.get(1);

                    // Getting URL to the Google Directions API
                    String url = getMapsApiDirectionsUrl(origin, dest);
                    // here code to draw route on mMap
                   // Log.d("onMapClick", url.toString());
                    ReadTask downloadTask = new ReadTask(getApplicationContext());

                    // Start downloading json data from Google Directions API
                    downloadTask.execute(url);
                }

            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
            // action with ID action_refresh was selected
            case R.id.action_refresh:
                finish();
                startActivity(getIntent());
                break;
            // action with ID action_settings was selected
            case R.id.action_settings:
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }
    }
    public void checkBackStack(){
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true); // show back button
                    myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                } else {
                    //show hamburger
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);

                    myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                }
            }
        });
    }
    private boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    protected boolean isConnected(Context context){
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }
    // get Url for getting data
    private String  getMapsApiDirectionsUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;


        return url;

    }
    class GetLatLong extends AsyncTask<String,Integer,Address> {
        private Context context;

        public GetLatLong(Context context){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Address doInBackground(String... params) {
            //Do some task
            Address address = null;
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List addressList = null;
            try {
                addressList = geocoder.getFromLocationName(params[0], 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addressList != null && addressList.size() > 0) {
                address = (Address) addressList.get(0);
            }

            publishProgress (1);

            return address;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            //Update the progress of current task
        }
        @Override
        protected void onPostExecute(Address latLng) {
            // get lat lng and put on the map to zoom camera
            LatLng centerPoint = new LatLng(latLng.getLatitude(),latLng.getLongitude());

            initialProgressBar.setVisibility(View.GONE);
            mainContent.setVisibility(View.VISIBLE);

            // move camera to the center of the city
            mMap.moveCamera(CameraUpdateFactory.newLatLng(centerPoint));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(12));

        }
    }

    private class ReadTask extends AsyncTask<String, Void , String> {
        private Context context;

        public ReadTask(Context context){
            this.context = context;
        }
        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String data = "";
            try {
                MapHttpConnection http = new MapHttpConnection();
                data = http.readUr(url[0]);


            } catch (Exception e) {
                // TODO: handle exception
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask(context).execute(result);
        }

    }
    private class ParserTask extends AsyncTask<String,Integer, List<List<HashMap<String , String >>>> {
        private Context context;

        public ParserTask(Context context){
            this.context = context;
        }
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {
            // TODO Auto-generated method stub
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            LatLng startLatLng = null, endLatLng = null;
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(12);
                polyLineOptions.color(Color.BLUE);
            }
            if(polyLineOptions != null){
                // draw polyline on map
                mMap.addPolyline(polyLineOptions);

                initialProgressBar.setVisibility(View.GONE);
                mainContent.setVisibility(View.VISIBLE);
            }
            else {
                Toast.makeText(context,"Cannot get data from google services, please hit refresh",Toast.LENGTH_LONG).show();
            }

        }
    }
    class GetAddressAndInsertRoute extends AsyncTask<LatLng,Integer,ArrayList<String>> {

        private Context context;

        private ArrayList<String> mappedAddresses;

        public GetAddressAndInsertRoute(Context context){
            this.context = context;
            mappedAddresses = new ArrayList<>();
        }
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected ArrayList<String> doInBackground(LatLng... params) {
            //Do some task
            List<Address> address;
            Geocoder  geocoder = new Geocoder(context, Locale.getDefault());
            for(int i =0 ;i < params.length; i++){
                try {
                    address = geocoder.getFromLocation(params[i].latitude, params[i].longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String nameOfAddress = "";
              //      System.out.println("Komplet Adresa: "+address.toString());
                    if(address.size() > 0){
                        nameOfAddress = address.get(0).getAddressLine(0);
                    }

                    mappedAddresses.add(nameOfAddress);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            return mappedAddresses;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            //Update the progress of current task
        }
        @Override
        protected void onPostExecute(ArrayList<String> stringAddresses) {
            // here you got the full address and retrieve start
            if(mappedAddresses != null && mappedAddresses.size() != 0){
                Route newRoute = new Route(stringAddresses.get(0),stringAddresses.get(1),selectedCity);
                db.addRoute(newRoute);
                Toast.makeText(context,"Route was inserted successfully",Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(context,"Cannot insert route into database, please try again",Toast.LENGTH_LONG).show();
            }
        }
    }
}
