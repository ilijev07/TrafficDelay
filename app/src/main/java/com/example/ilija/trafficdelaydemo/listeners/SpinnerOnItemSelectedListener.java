package com.example.ilija.trafficdelaydemo.listeners;


import android.os.Bundle;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ilija.trafficdelaydemo.R;
import com.example.ilija.trafficdelaydemo.fragments.DetailsCityFragment;


/**
 * Created by Ilija on 28-Jul-17.
 */

public class SpinnerOnItemSelectedListener implements AdapterView.OnItemSelectedListener {


    private FragmentActivity activity;
    private Spinner spinner;

    public SpinnerOnItemSelectedListener(FragmentActivity activity,Spinner spinner) {
        this.activity = activity;
        this.spinner=spinner;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedCity = parent.getItemAtPosition(position).toString();
        if(!selectedCity.equalsIgnoreCase("Select a city")) {
            spinner.setSelection(0);
            Bundle args = new Bundle();
            args.putString("selected_city", selectedCity);
            DetailsCityFragment detailsCityFragment = new DetailsCityFragment();
            detailsCityFragment.setArguments(args);
            FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.fragment_container, detailsCityFragment,"DetailsCityFragment");
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();

        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
