package com.example.ilija.trafficdelaydemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.ilija.trafficdelaydemo.fragments.RouteListFragment;
import com.example.ilija.trafficdelaydemo.fragments.SelectCityFragment;
import com.example.ilija.trafficdelaydemo.model.Route;

public class RoutesActivity extends AppCompatActivity implements RouteListFragment.OnFragmentInteractionListener{

    Toolbar myToolbar;
    String selectedCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routes);
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        checkBackStack();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        selectedCity = getIntent().getStringExtra("selectedCity");
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout




        if(!isConnected(this)){
            buildDialog(RoutesActivity.this).show();
        }
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            Bundle args = new Bundle();
            args.putString("selectedCity", selectedCity);


            // Create a new Fragment to be placed in the activity layout
            RouteListFragment routeListFragment = new RouteListFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            routeListFragment.setArguments(args);

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, routeListFragment,"RouteListFragment").commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
       // Log.w("Main ", "onStart");

    }

    @Override
    protected void onSaveInstanceState(Bundle bundle){
        super.onSaveInstanceState(bundle);
    }
    @Override
    protected void onStop() {
        super.onStop();
        // Log.w("Main ", "onStop");
    }
    @Override
    protected void onRestart() {
        super.onStop();
        // Log.w("Main ", "onRestart");
    }
    @Override
    protected void onPause() {
        super.onStop();
        // Log.w("Main ", "onPause");
    }
    @Override
    protected void onResume() {
        super.onStop();

        // Log.w("Main ", "onResume");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);
        if(isRouteListFragment()){
            menu.findItem(R.id.action_add_route).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
            // action with ID action_refresh was selected
            case R.id.action_refresh:
                refreshVisibleFragment();
                break;
            // action with ID action_settings was selected
            case R.id.action_add_route:
                // Start RoutesActivity.class
                Intent myIntent = new Intent(this,
                        NewRouteActivity.class);
                myIntent.putExtra("selectedCity",selectedCity);
                startActivity(myIntent);
                break;
            // action with ID action_settings was selected
            case R.id.action_settings:
                break;
            default:
                break;
        }

        return true;
    }

    protected boolean isConnected(Context context){
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }
    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or WiFi turned on for properly working of application." +
                " Press OK to turn them on");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent=new Intent(Settings.ACTION_SETTINGS);
                startActivity(intent);
            }
        });

        return builder;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                super.onBackPressed();

        }
    }
    public void checkBackStack(){
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true); // show back button
                    myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                } else {
                    //show hamburger
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);

                    myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                }
            }
        });
    }

    private void refreshVisibleFragment(){
        Fragment fragment =  getSupportFragmentManager().findFragmentByTag("RouteListFragment");

        if(fragment != null && fragment.isVisible()){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.detach(fragment);
            ft.attach(fragment);
            ft.commit();
        }

        Fragment fragment1 =  getSupportFragmentManager().findFragmentByTag("SelectCityFragment");

        if(fragment1 != null && fragment1.isVisible()){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.detach(fragment1);
            ft.attach(fragment1);
            ft.commit();
        }
    }

    private boolean isRouteListFragment(){
        Fragment fragment =  getSupportFragmentManager().findFragmentByTag("RouteListFragment");

        if(fragment != null && fragment.isVisible()){
           return true;
        }
        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
