package com.example.ilija.trafficdelaydemo.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ilija.trafficdelaydemo.R;
import com.example.ilija.trafficdelaydemo.RoutesActivity;
import com.example.ilija.trafficdelaydemo.model.CityDto;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailsCityFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetailsCityFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsCityFragment extends Fragment {

    // TODO: Rename and change types of parameters
    private String selectedCity;
    private ProgressBar progressBar;
    LinearLayout detailsLayout;
    Button viewRoutesButton;

    private OnFragmentInteractionListener mListener;

    public DetailsCityFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailsCityFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailsCityFragment newInstance(String param1, String param2) {
        DetailsCityFragment fragment = new DetailsCityFragment();

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        Log.w("DEBUG:","DetailsCityFragment onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.w("DEBUG:","DetailsCityFragment onCreate");
        if (getArguments() != null) {
            selectedCity = getArguments().getString("selected_city");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.w("DEBUG:","DetailsCityFragment onCreateView");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details_city, container, false);
        detailsLayout = (LinearLayout) view.findViewById(R.id.detailsLayout);
        detailsLayout.setVisibility(View.GONE);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar1);
        progressBar.setVisibility(View.VISIBLE);


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        Log.w("DEBUG:","DetailsCityFragment onActivityCreated");
        if(!isNetworkAvailable(getActivity())){
            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
        }
        else{
            new DbpediaData().execute(selectedCity);
        }

        viewRoutesButton = (Button) getActivity().findViewById(R.id.viewRoutesButton);

        // Capture button clicks
        viewRoutesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                // Start RoutesActivity.class
                Intent myIntent = new Intent(getActivity(),
                        RoutesActivity.class);
                myIntent.putExtra("selectedCity",selectedCity);
                startActivity(myIntent);
            }
        });

    }

    @Override
    public void onStart() {
        Log.w("DEBUG:","DetailsCityFragment onStart");
        super.onStart();

    }
    @Override
    public void onResume() {

        Log.w("DEBUG:","DetailsCityFragment onResume");
        super.onResume();
    }


    @Override
    public void onPause() {
        Log.w("DEBUG:","DetailsCityFragment onPause");
        super.onPause();
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class DbpediaData extends AsyncTask<String,Integer,CityDto> {


        @Override
        protected void onPreExecute() {


        }

        @Override
        protected CityDto doInBackground(String... params) {
            //Do some task

            String queryString=
                    "prefix dbo:  <http://dbpedia.org/ontology/>"+
                            "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
                            "prefix dbr:  <http://dbpedia.org/resource/>"+
                            "prefix dbp:  <http://dbpedia.org/property/>"+
                            "select ?label ?countryName ?population ?thumbnail ?comment \n" +
                            "where { "+
                            "dbr:"+params[0]+" rdfs:label ?label . \n"+
                            "dbr:"+params[0]+" dbo:country ?country . \n"+
                            "?country rdfs:label ?countryName. \n"+
                            "dbr:"+params[0]+" dbo:populationTotal ?population . \n"+
                            "dbr:"+params[0]+" dbo:thumbnail ?thumbnail .\n "+
                            "dbr:"+params[0]+" rdfs:comment ?comment .\n"+
                            "FILTER( lang(?label) = \"en\" ) . \n"+
                            "FILTER ( lang(?countryName ) =  \"en\" ) . \n"+
                            "FILTER ( lang(?comment ) =  \"en\" ) .}";


            // now creating query object
            Query query = QueryFactory.create(queryString);
            // initializing queryExecution factory with remote service.
            // **this actually was the main problem I couldn't figure out.**
            QueryExecution qexec = QueryExecutionFactory.sparqlService("https://dbpedia.org/sparql", query);

            //after it goes standard query execution and result processing which can
            // be found in almost any Jena/SPARQL tutorial.
            CityDto cityDto = new CityDto();
                ResultSet results = qexec.execSelect();

                for (; results.hasNext();) {
                    QuerySolution soln = results.nextSolution() ;
                    RDFNode commentNode = soln.get("comment") ;
                    RDFNode labelNode = soln.get("label") ;
                    RDFNode countryNameNode = soln.get("countryName") ;
                    RDFNode populationTotalNode = soln.get("population") ;
                    RDFNode thumbnailNode = soln.get("thumbnail") ;

                    cityDto.setComment(commentNode.asNode().getLiteral().getLexicalForm());
                    cityDto.setLabel(labelNode.asNode().getLiteral().getLexicalForm());
                    cityDto.setCountryName(countryNameNode.asNode().getLiteral().getLexicalForm());
                    cityDto.setPopulationTotal(populationTotalNode.asNode().getLiteral().getLexicalForm());
                    cityDto.setThumbnail(thumbnailNode.toString());;



                  //  System.out.println(cityDto.toString());

                }

                qexec.close();

            publishProgress (1);

            return cityDto;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            //Update the progress of current task
        }

        @Override
        protected void onPostExecute(CityDto cityDto) {
            //Show the result obtained from doInBackground
            progressBar.setVisibility(View.GONE);
            try {
                attachDataToView(getView(),cityDto,getActivity());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(cityDto.getThumbnail() == null || cityDto.getComment() == null || cityDto.getCountryName() == null ||
                    cityDto.getLabel() == null || cityDto.getPopulationTotal() == null){
                Toast.makeText(getActivity(),"Cannot get data for this city, please go back and select another",Toast.LENGTH_LONG).show();
            }
            else{
                detailsLayout.setVisibility(View.VISIBLE);
            }


        }
    }
    private void attachDataToView(View view, CityDto cityDto, Activity activity) throws IOException {
        ImageView cityImage = (ImageView) view.findViewById(R.id.cityImage);
        if(cityDto.getThumbnail() != null){
            String imageUrl = cityDto.getThumbnail().replace("http", "https");
            setImageFromUrl(imageUrl,cityImage,activity);
        }


        TextView cityLabel = (TextView) view.findViewById(R.id.cityLabel);
        cityLabel.setText(cityDto.getLabel());

        TextView cityPopulation = (TextView) view.findViewById(R.id.population);
        cityPopulation.setText(cityDto.getPopulationTotal());

        TextView countryName = (TextView) view.findViewById(R.id.country);
        countryName.setText(cityDto.getCountryName());

        TextView comment = (TextView) view.findViewById(R.id.comment);
        String komentar = "\t" + cityDto.getComment();
        comment.setText(komentar);
    }
    public static void setImageFromUrl(String strUrl,ImageView imageView,Activity activity) throws IOException {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Picasso.with(activity)
                .load(strUrl)
                .resize(width,height/2)
                .into(imageView,new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                //Success image already loaded into the view
                            }

                            @Override
                            public void onError() {
                                //Error placeholder image already loaded into the view, do further handling of this situation here
                            }
                        }
                );
        Picasso.with(activity).
                setLoggingEnabled(true);

    }
    private boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
