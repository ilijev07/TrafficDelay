# Traffic delay demo


## Traffic delay demo e андроид апликација која служи за приказ на доцнењето на неколку поголеми градови во Европа. 

### Краток опис на апликацијата: 

#####	Екран 1 - Иницијалниот екран прикажува листа која ни овозможува да одбереме некој од предефинираните градови. Во горниот десен агол постои копче за освежување на листата. Со избор на некој од градовите преминуваме во следен fragment.
 
   <p align="center"><img src="https://gitlab.com/ilijev07/TrafficDelay/raw/master/images/screen1.png" /></p>

#####	Екран 2 - Следен екран е екранот за приказ на детали од избраниот град, се прикажува неговото име, држава во која се наоѓа, неговата популација и краток опис на истиот по што е карактеристичен. Сите податоци се извлекуваат од Dbpedia. На овој fragment пости и копче “VIEW ROUTES” кое доколку се кликне преминуваме во друг фрагмент.
 
   <p align="center"><img src="https://gitlab.com/ilijev07/TrafficDelay/raw/master/images/screen2.png" /></p>

#####	Екран 3 - Следно е прикажување на рутите на истоимениот град. Ова е fragment од претходното activity нај горе на менито постојат копчиња на освежување на fragment-от, но и копче со кое се додава нова рута и не носи на Екран 5 од овој опис. Потоа се состои од името на градот, порака која го прикажува доцнењето за градот и копче “CALCULATE” кое ја врши пресметката. Подолу се прикажуваат сите рути од база кои се додадени од корисникот и случајно генерирани 10 рути од централната точка на градот во опсег од 1 до 10 км. За секоја рута во листата постои име на почетната точка from и име на крајната точка to, и копче “VIEW ON MAP” со кое преминуваме на следниот екран, екран 4.
 
   <p align="center"><img src="https://gitlab.com/ilijev07/TrafficDelay/raw/master/images/screen3.png" /></p>

#####	Екран 4 – Детален приказ на поединечната рута, нај горе на менито се наоѓа копче за освежување на екранот и копче за бришење на рутата ако е од база, односно ако истата е внесена од корисникот. Следно се состои од името на градот, порака за тоа колку е доцнењето на оваа рута со копче “CALCULATE” кое го пресметува истото, Slider со кој се вклучуваат Traffic lines на мапата која се наоѓа најдолу од овој екран. На неа се прикажани почетната точка на рутата Start point и крајната точка на рутата End point и е исцртан патот помеѓу двете точки.
 
   <p align="center"><img src="https://gitlab.com/ilijev07/TrafficDelay/raw/master/images/screen4.png" /></p>

#####	Екран 5 – Со кликање на копчето за додавање на нова рута на менито на Екран 3 стигнуваме до Екран 5 кој ни овозможува да внесеме нова рута. На истиот се наоѓа порака која ни го кажува начинот на внесување на рутата, копче за внесување на рутата која е оневозможено за кликање се додека не се одберат две точки на рутата кои ќе претставуваат почетна и крајна точка за одредена рута. Со клик на истото копче ја внесуваме рутата во базата на податоци која ќе се повлече при наредниот пристап до Екран 3.
 
   <p align="center"><img src="https://gitlab.com/ilijev07/TrafficDelay/raw/master/images/screen5.png" /></p>

